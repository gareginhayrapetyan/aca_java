package lesson0.binary;

import static lesson0.binary.BinaryUtil.showBitsOf;

/**
 * Stores 64 boolean flags in single 64 bit field
 */
public final class BitSet {
   private long state = 0;

   /**
    * Update flag at specified position with specified value
    */
   public void set(boolean value, int position) {
      if (value) {
         // if we want to set 1
         int mask = 1 << position;
         state = state | mask;
      } else {
         // if we want to set 0
         int mask = ~(1 << position);
         state = state & mask;
      }
   }

   /**
    * Get flag value at specified position
    */
   public boolean get(int position) {
      int mask = 1 << position;
      return (state & mask) != 0;
   }

   /**
    * Flip flag value at specified position
    */
   public void flip(int position) {
      int mask = 1 << position;
      state = state ^ mask;
   }

   @Override
   public String toString() {
      return "BitSet[ " + showBitsOf(state) + " ]";
   }
}


