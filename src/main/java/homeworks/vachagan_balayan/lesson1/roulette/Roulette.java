package homeworks.vachagan_balayan.lesson1.roulette;

import lesson1._roulette.interfaces.IRoulette;

import java.util.concurrent.ThreadLocalRandom;

public class Roulette implements IRoulette {

   private static class Result implements IResult {
      private final int number;

      public Result(int number) {
         this.number = number;
      }

      @Override
      public Color getColor() {
         if (number == 0) {
            return Color.GREEN;
         } else if (number % 2 == 0) {
            return Color.BLACK;
         } else {
            return Color.RED;
         }
      }

      @Override
      public int getNumber() {
         return number;
      }
   }

   @Override
   public IResult spin() {
      int number = ThreadLocalRandom.current().nextInt(0, 37);
      return new Result(number);
   }
}
