package lesson1.typesOfClasses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

interface I {
   void m1();

   void m2();
}

abstract class A implements I {

   @Override
   public void m1() {
      // implements some of the methods but not all
   }
}

final class B extends A {

   @Override
   public void m2() {
      // derives abstract
   }
}

class C implements I {

   @Override
   public void m1() {

   }

   @Override
   public void m2() {

   }
}

enum Colour {
   RED(1, 2, 3),
   GREEN(1, 2, 4),
   BLUE(1, 3, 4);

   int r;
   int g;
   int b;

   Colour(int r, int g, int b) {
      this.r = r;
      this.g = g;
      this.b = b;
   }
}

class OuterClass {
   int f1;

   void m1() {

   }

   static class StaticNestedClass {
      int f2;
   }

   class InnerClass {
      int f1;

      void m1() {
      }
   }

   void localInnerClass() {

      class LocalInnerClass {
      }

   }

   void anonInnerClass() {

      I instanceOfI = new I() {
         @Override
         public void m1() {

         }

         @Override
         public void m2() {

         }
      };
   }
}

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@interface Metadata {
   String author();

   String version();
}

@Metadata(
   author = "Vachagan",
   version = "0.1"
)
class AnnotatedType {

   @Metadata(
      author = "Vachagan",
      version = "0.1"
   )
   void m1() {

   }
}